variable "address" {}
variable "token" {}
variable "description" { default = "" }

variable "intermediate_ca_private_key_file" {}
variable "intermediate_ca_certificate_file" {}
variable "root_ca_certificate_file" {}
