resource "vault_pki_secret_backend_config_ca" "intermediate" {
  depends_on = [vault_mount.intermediate]

  backend = vault_mount.intermediate.path

  pem_bundle = join("\n", [
    chomp(file(pathexpand(var.intermediate_ca_private_key_file))),
    chomp(file(pathexpand(var.intermediate_ca_certificate_file))),
    chomp(file(pathexpand(var.root_ca_certificate_file))),
  ])
}
