output "pki_server_role_name" {
  value = vault_pki_secret_backend_role.server.name
}

output "pki_client_role_name" {
  value = vault_pki_secret_backend_role.client.name
}

output "pki_backend_path" {
  value = vault_mount.intermediate.path
}

