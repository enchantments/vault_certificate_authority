resource "vault_mount" "intermediate" {
  path        = "pki"
  type        = "pki"
  description = coalesce(var.description, "The is the organization intermediate certificate authority.")
}
