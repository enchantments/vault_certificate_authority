resource "vault_pki_secret_backend_role" "server" {
  backend = vault_mount.intermediate.path
  name    = "server"

  key_type           = "ec"
  key_bits           = 384
  allow_any_name     = true
  enforce_hostnames  = false
  require_cn         = false
  client_flag        = true
  server_flag        = true

  key_usage = [
    "DigitalSignature",
    "KeyAgreement",
    "KeyEncipherment",
  ]
}
