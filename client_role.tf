resource "vault_pki_secret_backend_role" "client" {
  backend = vault_mount.intermediate.path
  name    = "client"

  key_type           = "ec"
  key_bits           = 384
  allow_any_name     = true
  enforce_hostnames  = false
  require_cn         = false
  client_flag        = true
  server_flag        = false

  key_usage = [
    "DigitalSignature",
    "KeyAgreement",
    "KeyEncipherment",
  ]
}
