data "vault_policy_document" "issue_server_pki" {
  rule {
    path         = "pki/sign/server"
    capabilities = ["create", "update"]
    description  = "Allow issuing server TLS certificates."
  }

  rule {
    path         = "auth/token/create"
    capabilities = ["create", "update"]
    description  = "Allow creating limited child token."
  }
}

resource "vault_policy" "issue_server_pki" {
  name   = "issue-server-pki"
  policy = data.vault_policy_document.issue_server_pki.hcl
}
